<!DOCTYPE html>
<html lang="id" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
        <meta name="description" content="Muse.id adalah platform berbagi untuk sesama">

        <title>Muse ID | @yield('title')</title>

        <link rel="shortcut icon" href="{{ asset('img/muse-logo.png') }}">

        <!-- <link rel="stylesheet" href="{{ asset('lib/bootstrap.min.css') }}"> -->
        <link rel="stylesheet" href="{{ asset('css/nav.css') }}">
        
        <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/base.css') }}">

        @yield('extra-fonts')
        @yield('prerender-js')
        @yield('extra-css')
    </head>
    <body>
        @include('layouts/_navbar')
        @yield('content')
        @include('layouts/_footer')
        
        <script src="{{ asset('lib/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('lib/popper.min.js') }}"></script>
        <script src="{{ asset('lib/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/base.js') }}"></script>
        @yield('extra-js')
    </body>
</html>
