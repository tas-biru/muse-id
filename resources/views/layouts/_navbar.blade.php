
<nav>
    <ul>
    <a href="{{ route('halamanDepan') }}">
        <img style="height: 24px; margin: 12px;" src="{{ asset('img/logo-text.png')}}">
    </a>
    @guest
    <li><a href="{{ route('register.pilihRole') }}">Login/Register</a></li>
    @else
    <li>
        <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
    @endguest
    <li><a href="{{ route('toko.cari') }}">Toko Barang</a></li>
    <li><a href="{{ route('usul.formulir') }}">Usul Penerima</a></li>
    <li><a href="{{ route('relawan.tentang') }}">Relawan</a></li>
    <li><a href="{{ route('donasi.formulir.makanan') }}">Donasi Makanan</a></li>
    <li><a href="{{ route('donasi.formulir.barang') }}">Donasi Barang</a></li>
    </ul>

</nav>
