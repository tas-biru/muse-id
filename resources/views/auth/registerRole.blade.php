@extends('layouts/base')

@section('title', 'Platform Berbagi Bersama')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')
    <div class="container-fluid">
    	<div class="row">
    		<div class="col-md-6">
                <div style="height: 200px; width: 400px; margin-left: auto; margin-right: auto" class="card role p-4">
    				<img style="width: 100px; margin-left: auto; margin-right: auto" src="{{ asset('img/donatur-role.png')}}">
                    <br>
                    <h3 class="text-center font-weight-bold">
                        <a href="{{ route('register.formulir', ['role' => 'donatur']) }}">
                            Donatur
                        </a>
                    </h3>
                </div>
            </div>

    		<div class="col-md-6">
                <div style="height: 200px; width: 400px; margin-left: auto; margin-right: auto" class="card role p-4">
    				<img style="width: 100px; margin-left: auto; margin-right: auto" src="{{ asset('img/penerima-role.png')}}">
                    <br>
                    <h3 class="text-center font-weight-bold">
                        <a href="{{ route('register.formulir', ['role' => 'penerima']) }}">
                            Penerima
                        </a>
                    </h3>
                </div>
    		</div>
    	</div>
        <br>
        <div class="row w-100">
            <a class="w-100" href="{{ route('login') }}">
                <p class="w-100" style="text-align:center;">Sudah punya akun? Login</p>
            </a>
        </div>
    </div>
@endsection

@section('extra-js')

@endsection
