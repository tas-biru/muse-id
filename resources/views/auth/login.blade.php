@extends('layouts/base')

@section('title', 'Login')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')


<div class="container-fluid">

	<div class="row">

		<div class="col-md-6 register-panel" >
			<h2 class="text-center font-weight-bold">
				Login
			</h2>

			<form method="POST" action="{{ route('login') }}">
				@csrf
				<div class="form-group">
					<label for="inputJenisPangan">
						Email
					</label>
					<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" focus type="text" id="inputJenisPangan">
				</div>

				<div class="form-group">
					<label for="inputKeterangan">
						Password
					</label>
					<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" id="inputKeterangan">
				</div>

                <button type="submit" class="primary-btn large-btn">LOGIN</button>

			</form>
		</div>
	</div>
</div>
@endsection

@section('extra-js')

@endsection
