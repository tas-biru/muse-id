@extends('layouts/base')

@section('title', 'Relawan')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')
<div class="container-fluid">

	<div class="row">
		<div class="col-md-6">
		<div class="white-space"></div>

			<h2>
				Relawan MUSE.ID
			</h2>
			<br>
			<p><b>#AyoJadiMuseHeroes</b><br>
			Dengan menjadi relawan, dapatkan pengalaman berharga untuk membantu Muse.id menjemput barang donasi dari donatur dan menyalurkan donasi yang sudah diberikan kepada masyarakat pra-sejahtera di Jakarta.
			<br><br>
			<button onclick="location.href='{{ route('relawan.formulir') }}'" class="primary-btn large-btn">DAFTAR</button>

			</div>
		<div class="col-md-6 pl-5">
			<img style="height: 500px;" src="{{ asset('img/relawan1.png')}}">
		</div>
	</div>

	<div style="height: 128px" class="white-space"></div>

	<h2>
				Manfaat Menjadi Relawan
	</h2>
	<br><br>
	<div class="row">
		<div class="col-md-3">
			<div class="card2">
				<div class="card-body"><center>
				<img class="relawan-img" src="{{ asset('img/icon1.png')}}">
				<br><br>
				<p>Mengasah kepekaan sosial dengan berinteraksi langsung dengan warga</p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card2">
				<div class="card-body"><center>
				<img class="relawan-img" src="{{ asset('img/icon2.png')}}">
				<br><br>
				<p>Berkontribusi memerangi sampah makanan</p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card2">
				<div class="card-body"><center>
				<img class="relawan-img" src="{{ asset('img/icon3.png')}}">
				<br><br>
				<p>Membantu mengurangi kemiskinan </p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card2">
				<div class="card-body"><center>
				<img class="relawan-img" src="{{ asset('img/icon4.png')}}">
				<br><br>
				<p>Berkontribusi untuk membantu sesama</p>
				</div>
			</div>
		</div>
	</div>
	<div style="height: 128px" class="white-space"></div>
	<div class="container">
		<img class="full-img" src="{{ asset('img/relawan2.png')}}">
  		<button onclick="location.href='{{ route('relawan.formulir') }}'" class="btn primary-btn large-btn">DAFTAR SEKARANG</button>
	</div>




</div>
@endsection

@section('extra-js')

@endsection
