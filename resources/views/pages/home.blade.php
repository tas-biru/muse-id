@extends('layouts/base')

@section('title', 'Platform Berbagi Bersama')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h1>
				MUSE.ID
			</h1>
			<p style="width: 700px">
				<b>MUSE.ID</b> membantu pengguna untuk mendonasikan barang dan pangan berlebih yang mereka miliki, serta dengan serangkaian uji kelayakan makanan untuk disalurkan pada masyarakat pra-sejahtera (kaum dhuafa, yatim piatu, janda, lansia, difabel, pengungsi, dan anak jalanan) di Jakarta.			</p>
		</div>
	</div>

	<div style="height: 48px" class="white-space"></div>
	<div class="row">
		<div class="col-md-4">
			<div style="height: 410px" class="card">
				<div class="card-body">
				<img class="full-img" src="{{ asset('img/card1.png')}}">
				<br><br>
				<h4>Donasi Pangan</h4>
					<p>
					Donasikan bahan pangan atau makanan berlebih yang kamu ingin kamu bagikan.
					</p>
					<button onclick="location.href='{{ route('donasi.formulir.makanan') }}'" class="primary-btn med-btn right-btn">DONASI</button>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div style="height: 410px" class="card">
				<div class="card-body">
				<img class="full-img" src="{{ asset('img/card2.png')}}">
				<br><br>
				<h4>Donasi Barang</h4>
					<p>
					Donasikan barang layak pakai, yang sudah tidak kamu gunakan, seperti pakaian, buku, furnitur, koran, dll.					</p>
					<button onclick="location.href='{{ route('donasi.formulir.barang') }}'" class="primary-btn med-btn right-btn">DONASI</button>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div style="height: 410px" class="card">
				<div class="card-body">
				<img class="full-img" src="{{ asset('img/card3.png')}}">
				<br><br>
				<h4>Toko Barang</h4>
					<p style="color: #233B6E; font-size:13px; margin-top: -10px;">(Khusus pemilik Kartu Keluarga Sejahtera) </p>
					<p style="margin-top: -15px;">
					Dapatkan pilihan barang gratis dengan menukarkan poin yang kamu miliki.					</p>
					<button onclick="location.href='{{ route('toko.cari') }}'" class="primary-btn med-btn right-btn">KUNJUNGI</button>
				</div>
			</div>
		</div>
	</div>

	<div class="white-space"></div>

	<div class="row">
		<div class="col-md-4">
			<img style="width: 300px;" src="{{ asset('img/home1.png')}}">
		</div>
		<div class="col-md-8 pl-5">
			<h2>
				Tentang Kami
			</h2>
			<br>
			<p style="width: 500px;">
			Muse.id didirikan pada tahun 2019 oleh tiga orang pemuda dengan memiliki visi untuk meningkatkan kesejahteraan masyarakat Jakarta, terutama masyarakat pra-sejahtera yang belum memiliki akses untuk mencukupi kebutuhan sandang, pangan, dan papan mereka dengan mudah. Muse.id bertujuan meminimalkan dan membantu menurunkan tingkat kemiskinan masyarakat Jakarta, serta menumbukan empati kepada masyarakat yang sudah berkecukupan untuk saling berbagi kepada sesama.			</p>

		</div>
	</div>

	<div class="white-space"></div>

	<div class="row">
		<div class="col-md-6">
		<div class="white-space"></div>

			<h2>
				Relawan MUSE.ID
			</h2>
			<br>
			<p><b>#AyoJadiMuseHeroes</b><br>
			Dengan menjadi relawan, dapatkan pengalaman berharga untuk membantu Muse.id menjemput barang donasi dari donatur dan menyalurkan donasi yang sudah diberikan kepada masyarakat pra-sejahtera di Jakarta.
			<br><br>
			<button onclick="location.href='{{ route('relawan.tentang') }}'" class="primary-btn large-btn">DAFTAR</button>

			</div>
		<div class="col-md-6 pl-5">
			<img style="height: 500px;" src="{{ asset('img/home2.png')}}">
		</div>
	</div>

	<div class="white-space"></div>


	<div class="row">
		<div class="col-md-5">
			<img style="width: 400px;" src="{{ asset('img/home3.png')}}">
		</div>
		<div class="col-md-7 pl-5">
			<h2>
				Usulkan Penerima
			</h2>
			<br>
			<p clas="pl-5" style="width: 500px;">
			Bantu kami menyalurkan donasi yang Muse.id terima kepada cakupan masyarakat pra-sejahtera yang lebih luas di Jakarta, dengan cara memberikan rekomendasi masyarakat pra-sejahtera atau lokasi  yang membutuhkan bantuan. Usulan dan rekomendasi kamu akan sangat membantu Muse.id menyalurkan donasi dengan tepat sasaran.
			<br><br>
			<button onclick="location.href='{{ route('usul.formulir') }}'" style="width: 280px;" class="primary-btn large-btn">BERIKAN USUL</button>
		</div>
	</div>
</div>
@endsection

@section('extra-js')

@endsection
