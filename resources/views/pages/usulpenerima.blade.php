@extends('layouts/base')

@section('title', 'Usul Penerima')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')

<div class="container-fluid">
	<form method="POST" action="{{ route('usul.submit') }}">
		@csrf
		<div class="row">
			<div class="col-md-6">
	            <img class="full-img" src="{{ asset('img/usul.png')}}">
			</div>
			<div class="col-md-6 white-panel" >
				<h2 class="text-center font-weight-bold">
					Usul Penerima
				</h2>

				<div class="form-group">
					<label for="inputJenisPangan">
						Nama lokasi
					</label>
					<input name="nama_lokasi" required focus type="text" class="form-control" id="inputJenisPangan" placeholder="Panti Asuhan Kasih Ibu">
				</div>
				<div class="form-group">
					<label for="inputJumlahPorsi">
						Alamat
					</label>
					<input nama="alamat" required type="text" class="form-control" id="inputJumlahPorsi" placeholder="Jl Sudirman No. 250 Jakarta">
                </div>
                <div class="form-group">
					<label for="inputKeterangan">
						Nama narahubung
					</label>
					<input name="nama_narahubung" type="text" class="form-control" id="inputKeterangan" placeholder="Brad Pitt">
				</div>
				<div class="form-group">
					<label for="inputKeterangan">
						Telepon
					</label>
					<input name="telepon" type="tel" class="form-control" id="inputKeterangan" placeholder="(239) 555-0108">
				</div>
				<div class="form-group">
					<label for="inputKeterangan">
						Perkiraan jumlah penerima
					</label>
					<input name="jumlah_penerima" type="number" class="form-control" id="inputKeterangan" placeholder="Sepuluh anak-anak">
				</div>
				<div class="form-group">
					<label for="inputKeterangan">
						Keterangan tambahan
					</label>
					<input name="keterangan" type="textarea" class="form-control" id="inputKeterangan" placeholder="Didekat daerah Kali Ciliwung">
				</div>
				<div>
					<label class="container">Dengan ini, saya menyatakan data yang saya isi adalah benar dan valid
  					<input type="checkbox" required>
  					<span class="checkmark"></span>
					</label>
				</div>
                <button type="submit" class="primary-btn large-btn">KIRIM</button>

			</div>
		</div>
	</form>
</div>
@endsection

@section('extra-js')

@endsection
