@extends('layouts/base')

@section('title', 'Daftar Relawan')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
            <img class="full-img" src="{{ asset('img/relawan.png')}}">
		</div>
		<div class="col-md-6 white-panel" >
			<h2 class="text-center font-weight-bold">
				Daftar menjadi Relawan
			</h2>

			<form method="POST" action="{{ route('relawan.daftar') }}">
				@csrf
				<div class="form-group">
					<label for="inputJenisPangan">
						Nama lengkap
					</label>
					<input name="name" required focus type="text" class="form-control @error('name') is-invalid @enderror" id="inputJenisPangan" value="{{ old('name') }}" autocomplete="name" placeholder="Bernard Jones">
					@error('name')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="inputJumlahPorsi">
						Nomor telepon
					</label>
					<input required type="tel" class="form-control @error('telepon') is-invalid @enderror" name="telepon" id="inputJumlahPorsi" value="{{ old('telepon') }}" placeholder="(239) 555-0108">
					@error('telepon')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
                <div class="form-group">
					<label for="inputKadaluarsa">
						Alamat
					</label>
					<input name="alamat" required type="text" class="form-control" id="inputKadaluarsa" placeholder="Jl Margonda Raya No 71">
					@error('alamat')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
                <div class="form-group">
					<label for="inputKeterangan">
						Email
					</label>
					<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" id="inputKeterangan" placeholder="john@doe.com" autocomplete="email">
					@error('email')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div>
					<label class="container">Dengan ini, saya menyatakan data yang saya isi adalah benar dan valid
  					<input type="checkbox" required>
  					<span class="checkmark"></span>
					</label>
				</div>
                <div class="button-group">
                    <button type="button" class="secondary-btn large-btn" onclick="location.href='{{ URL::previous() }}'">KEMBALI</button>
                    <button type="submit" class="primary-btn large-btn">DAFTAR</button>
                </div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('extra-js')

@endsection
