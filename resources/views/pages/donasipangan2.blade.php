@extends('layouts/base')

@section('title', 'Donasi Pangan')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')


<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
            <img class="full-img" src="{{ asset('img/donasipangan.png')}}">
		</div>
		<div class="col-md-6 white-panel" >
			<h2 class="text-center font-weight-bold">
				Data Donatur
			</h2>
            <img style="width:200px; margin-left: 25%; margin-right: 25%;"src="{{ asset('img/progress2.png')}}">
            <br><br>
             
			<form role="form">
				<div class="form-group">
					<label for="inputJenisPangan">
						Nama donatur
					</label>
					<input required focus type="text" class="form-control" id="inputJenisPangan" placeholder="John Doe">
				</div>
				<div class="form-group">
					<label for="inputJumlahPorsi">
						Nomor telepon
					</label>
					<input required type="text" class="form-control" id="inputJumlahPorsi" placeholder="0878123456789">
                </div>
                <div class="form-group">
					<label for="inputKadaluarsa">
						Alamat
					</label>
					<input required type="date" class="form-control" id="inputKadaluarsa" placeholder="Jl Sudirman No. 250 Jakarta">
                </div>
                <div class="form-group">
					<label for="inputKeterangan">
						Email
					</label>
					<input type="textarea" class="form-control" id="inputKeterangan" placeholder="john@doe.com">
				</div>

				<div>
					<label class="container">Dengan ini, saya menyatakan data yang saya isi adalah benar dan valid
  					<input type="checkbox">
  					<span class="checkmark"></span>
					</label>
				</div>
				
                <div class="button-group">
                    <button type="button" class="secondary-btn large-btn">KEMBALI</button>
                    <button type="button" class="primary-btn large-btn">KIRIM</button>
                </div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('extra-js')

@endsection
