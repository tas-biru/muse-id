@extends('layouts/base')

@section('title', 'Registrasi Donatur')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')


<div class="container-fluid">

	<div class="row">

		<div class="col-md-6 register-panel" >
			<h2 class="text-center font-weight-bold">
				Registrasi Penerima
			</h2>

			<form method="POST" action="{{ route('register') }}">
				@csrf
				<div class="form-group">
					<label for="inputJenisPangan">
						Username
					</label>
					<input class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" focus type="text" id="inputJenisPangan" placeholder="Bernard Jones">
					@error('name')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="inputJumlahPorsi">
						Telepon
					</label>
					<input type="tel" class="form-control @error('telepon') is-invalid @enderror" name="telepon" value="{{ old('telepon') }}" required id="inputJumlahPorsi" placeholder="(239) 555-0108">
					@error('telepon')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
                <div class="form-group">
					<label for="inputKeterangan">
						Email
					</label>
					<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" id="inputKeterangan" placeholder="john@doe.com">
					@error('email')
					    <span class="invalid-feedback" role="alert">
					        <strong>{{ $message }}</strong>
					    </span>
					@enderror
				</div>
				<div class="form-group">
					<label for="password">
						Password
					</label>
					<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
					@error('password')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
				</div>
				<div>
					<label class="container">Dengan ini, saya menyatakan data yang saya isi adalah benar dan valid
  					<input type="checkbox" required>
  					<span class="checkmark"></span>
					</label>
				</div>
                <button type="submit" class="primary-btn large-btn">DAFTAR</button>

			</form>
		</div>
	</div>
</div>
@endsection

@section('extra-js')

@endsection
