@extends('layouts/base')

@section('title', 'Donasi Barang')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')


<div class="container-fluid">
	<form method="POST" action="{{ route('donasi.submit.barang') }}">
		@csrf
	<div class="row">
		<div class="col-md-6">
            <img class="full-img" src="{{ asset('img/donasibarang.png')}}">
		</div>

		<div class="col-md-6 white-panel tab" >
			<h2 class="text-center font-weight-bold">
				Detail Donasi
			</h2>
	        <img style="width:200px; margin-left: 25%; margin-right: 25%;"src="{{ asset('img/progress1.png')}}">
	        <br><br>

			<div class="form-group">
				<label for="inputJenisPangan">
					Jenis barang
				</label>
				<input name="jenis" required focus type="text" class="form-control" id="inputJenisPangan" placeholder="Baju dan celana bekas">
			</div>
			<div class="form-group">
				<label for="inputJumlahPorsi">
					Jumlah barang
				</label>
				<input name="jumlah" required type="number" class="form-control" id="inputJumlahPorsi" placeholder="14 pcs">
            </div>
            <div class="form-group">
				<label for="inputKadaluarsa">
					Keterangan tambahan (opsional)
				</label>
				<input name="keterangan" required type="text" class="form-control" id="inputKadaluarsa" placeholder="Pakaian bekas dewasa">
            </div>
			<div style="height: 78px" class="white-space"></div>
            <div class="button-group">
                <button type="button" class="secondary-btn large-btn" onclick="location.href='{{ URL::previous() }}'">BATAL</button>
                <button type="button" class="primary-btn large-btn" onclick="nextPrev(1)">LANJUT</button>
            </div>
		</div>

		<div class="col-md-6 white-panel tab">
			<h2 class="text-center font-weight-bold">
				Data Donatur
			</h2>
            <img style="width:200px; margin-left: 25%; margin-right: 25%;"src="{{ asset('img/progress2.png')}}">
            <br><br>

			<div class="form-group">
				<label for="inputJenisPangan">
					Nama donatur
				</label>
				<input name="nama_donatur" value="{{ $user->name }}" required focus type="text" class="form-control" id="inputJenisPangan" placeholder="John Doe">
			</div>
			<div class="form-group">
				<label for="inputJumlahPorsi">
					Nomor telepon
				</label>
				<input name="telepon" value="{{ $user->telepon }}" required type="tel" class="form-control" id="inputJumlahPorsi" placeholder="0878123456789">
            </div>
            <div class="form-group">
				<label for="inputKadaluarsa">
					Alamat
				</label>
				<input name="alamat" value="{{ $user->alamat }}" required type="text" class="form-control" id="inputKadaluarsa" placeholder="Jl Sudirman No. 250 Jakarta">
            </div>
            <div class="form-group">
				<label for="inputKeterangan">
					Email
				</label>
				<input name="email" value="{{ $user->email }}" type="email" class="form-control" id="inputKeterangan" placeholder="john@doe.com">
			</div>

			<div>
				<label class="container">Dengan ini, saya menyatakan data yang saya isi adalah benar dan valid
					<input type="checkbox" required>
					<span class="checkmark"></span>
				</label>
			</div>

            <div class="button-group">
                <button type="button" class="secondary-btn large-btn" onclick="prevNext(1)">KEMBALI</button>
                <button type="submit" class="primary-btn large-btn">KIRIM</button>
            </div>
		</div>

	</div>
	</form>
</div>
@endsection

@section('extra-js')
	<script type="text/javascript">
		var currentTab = 0; // Current tab is set to be the first tab (0)
		(document.getElementsByClassName("tab"))[currentTab].style.transform = "scale(1,1)";
		(document.getElementsByClassName("tab"))[currentTab].style.height = "100%";
		function nextPrev(n) {
			var x = document.getElementsByClassName("tab");
			if (n == 1) {
				x[currentTab].style.display = "none";
				currentTab += n;
				console.log(currentTab);
				x[currentTab].style.display = "block";
				x[currentTab].style.transform = "scale(1,1)";
				x[currentTab].style.height = "100%";
			}
		}
		function prevNext(n) {
			var x = document.getElementsByClassName("tab");
			if (n == 1) {
				x[currentTab].style.display = "none";
				currentTab -= n;
				console.log(currentTab);
				x[currentTab].style.display = "block";
				x[currentTab].style.transform = "scale(1,1)";
				x[currentTab].style.height = "100%";
			}
		}
	</script>
@endsection
