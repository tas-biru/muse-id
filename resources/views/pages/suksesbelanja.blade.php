@extends('layouts/base')

@section('title', 'Konfirmasi')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')

@endsection

@section('content')


<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
            <div style="height: 530px; width: 500px; margin-left: auto; margin-right: auto" class="card p-4">
				<img style="width: 400px; margin-left: auto; margin-right: auto" src="{{ asset('img/suksesbelanja.png')}}">
                <br>
                <h5 class="text-center font-weight-bold">
                    Terima kasih telah berbelanja di Toko Barang. Barangmu akan dikirim dalam waktu tiga hari kerja.
                </h5>
                <br>
                <button  onclick="location.href='{{ route('halamanDepan') }}'" style="width: 250px; font-size: 12px;margin-left: auto; margin-right: auto" class="secondary-btn medium-btn">< Kembali ke home</button>
            </div>
        </div>
	</div>
</div>
@endsection

@section('extra-js')

@endsection
