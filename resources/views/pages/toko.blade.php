@extends('layouts/base')

@section('title', 'Platform Berbagi Bersama')

@section('extra-fonts')
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
@endsection

@section('prerender-js')

@endsection

@section('extra-css')
<link rel="stylesheet" href="{{ asset('css/toko.css') }}">
@endsection

@section('content')
<div class="sidenav">
    <a href="#about">Semua Kategori</a>
    <a href="#services">Pakaian Pria</a>
    <a href="#clients">Pakaian Wanita</a>
    <a href="#contact">Buku</a>
    <a href="#contact">Furnitur</a>

    <br>
    <div class="line-divider"></div>
    <br>
    <h6>
        Poin kamu saat ini:
    </h6>
    <h4 id="jmlpoin">
        Poin:
    </h4>

    <h6>
        Total poin belanjaan:
    </h6>
    <h4 id="totalpoin">
        Poin:
    </h4>
    <br>

    <button onclick="tukarkanPoin()" class="primary-btn large-btn tukar-poin">TUKARKAN POIN</button>

</div>

<div class="container-fluid">
	<div class="row">
        <div class="col-md-2">

        </div>

		<div class="col-md-10 pl-4">

            <div class="flex">
                <div>
                <img style="height: 36px; margin-right: 20px;" src="{{ asset('img/filter.png')}}">
                <img style="height: 36px;"src="{{ asset('img/urutkan.png')}}">
                </div>
                <div>
                    <input class="search" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
                </div>
            </div>

            <div class="row pb-3">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko1.png')}}">
                        <h5 style="margin-top: 10px">Kaos warna hijau</h5>
                        <p style="line-height: 11px">
                        Kondisi: Cukup baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 id="poin" style="margin: 5px 0 0 -8px">55 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(1, 55)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko2.png')}}">
                        <h5 style="margin-top: 10px">Celana Jeans 32</h5>
                        <p style="line-height: 11px">
                        Kondisi: Kurang baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">60 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(2, 60)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko3.png')}}">
                        <h5 style="margin-top: 10px">Kemeja Kerja</h5>
                        <p style="line-height: 11px">
                        Kondisi: Cukup baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">45 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(3, 45)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko4.png')}}">
                        <h5 style="margin-top: 10px">Kaos warna hitam</h5>
                        <p style="line-height: 11px">
                        Kondisi: Baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">35 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(4, 35)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pb-3">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko5.png')}}">
                        <h5 style="margin-top: 10px">Kaos warna putih</h5>
                        <p style="line-height: 11px">
                        Kondisi: Kurang baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">25 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(5, 25)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko6.png')}}">
                        <h5 style="margin-top: 10px">Jaket Kulit</h5>
                        <p style="line-height: 11px">
                        Kondisi: Sangat baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">100 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(6, 100)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko7.png')}}">
                        <h5 style="margin-top: 10px">Kaos Bola Merah</h5>
                        <p style="line-height: 11px">
                        Kondisi: Cukup baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">45 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(7, 45)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko8.png')}}">
                        <h5 style="margin-top: 10px">Jaket Jeans</h5>
                        <p style="line-height: 11px">
                        Kondisi: Baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">80 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(8, 80)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pb-3">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko9.png')}}">
                        <h5 style="margin-top: 10px">Sepatu Putih</h5>
                        <p style="line-height: 11px">
                        Kondisi: Baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">90 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(9, 90)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko2.png')}}">
                        <h5 style="margin-top: 10px">Celana Jeans 36</h5>
                        <p style="line-height: 11px">
                        Kondisi: Cukup baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">65 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(10, 65)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko5.png')}}">
                        <h5 style="margin-top: 10px">Kaos warna kuning</h5>
                        <p style="line-height: 11px">
                        Kondisi: Sangat Baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">35 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(11, 35)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <img class="full-img" src="{{ asset('img/toko9.png')}}">
                        <h5 style="margin-top: 10px">Sepatu Biru</h5>
                        <p style="line-height: 11px">
                        Kondisi: Baik
                        </p>
                        <div class="row mb-1">
                            <div class="col-sm-2">
                            <img src="{{ asset('img/pricetag.png')}}">
                            </div>

                            <div class="col-sm-10">
                            <h6 style="margin: 5px 0 0 -8px">80 poin</h6>
                            </div>
                        </div>
                        <button style=" width:100%" onclick="increasePoin(12, 80)" class="secondary-btn med-btn">+ PILIH</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('extra-js')
<script src="{{ asset('js/toko.js') }}"></script>
<script type="text/javascript">
    var poin = document.getElementById("poin").value;
    var jmlpoin = document.getElementById("jmlpoin");
    var total = document.getElementById("totalpoin");
    var button = document.getElementById("pilih")
    @if( auth()->user() != null && auth()->user()->roles()->first()->name == 'penerima')
    var poinskr = 500;
    count = 0;
    @else
    count = "-";
    var poinskr = "-";
    @endif
    jmlpoin.innerHTML = poinskr;
    total.innerHTML = count;

    function increasePoin(n, poin) {
        var x = document.getElementsByClassName("med-btn");

        console.log(x[n-1].className);
        if (x[n-1].className == "secondary-btn med-btn") {
            count += poin;
            x[n-1].className = "primary-btn med-btn";
        } else {
            count -= poin;
            x[n-1].className = "secondary-btn med-btn";
        }
        total.innerHTML = count;
    }

    function tukarkanPoin() {
        if (poinskr - count >= 0) {
            poinskr -= count;
            jmlpoin.innerHTML = poinskr;
            count = 0;
            total.innerHTML = count;
            var x = document.getElementsByClassName("med-btn");
            for (i=0; i<100; i++) {
                x[i].className = "secondary-btn med-btn";
            }
        }
    }

    $( document ).ready(function() {
        var x = document.getElementsByClassName("med-btn");

        @if( auth()->user() == null || auth()->user()->roles()->first()->name != 'penerima')
        for (i=0; i<100; i++) {
            x[i].disabled = true;
        }
        @else
        for (i=0; i<100; i++) {
            x[i].disabled = false;
        }
        @endif
    });
</script>
@endsection
