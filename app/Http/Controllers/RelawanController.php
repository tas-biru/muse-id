<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Relawan;
use Illuminate\Http\Request;

/**
 *
 */

 class RelawanController extends Controller
 {
     public function tentang() {
         return view('pages.relawan');
     }

     public function formulir() {
         return view('pages.daftarrelawan');
     }

     public function daftar(Request $request) {
         $relawan = new Relawan;
         $relawan->name             = $request->name;
         $relawan->email            = $request->email;
         $relawan->alamat           = $request->alamat;
         $relawan->telepon          = $request->telepon;
         $relawan->save();

         return redirect()->route('sukses', ['message' => 'Selamat! Anda berhasil mendaftar menjadi relawan. Tunggu pengumuman berikutnya ya!']);
     }
}
