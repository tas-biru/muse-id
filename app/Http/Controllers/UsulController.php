<?php

namespace App\Http\Controllers;

use App\Usulan;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 *
 */
 class UsulController extends Controller
 {
     public function formulir() {
         return view('pages.usulpenerima');
     }

     public function submit(Request $request) {
         $usulan = new Usulan;
         $usulan->nama_lokasi             = $request->nama_lokasi;
         $usulan->nama_narahubung         = $request->nama_narahubung;
         $usulan->telepon                 = $request->telepon;
         $usulan->jumlah_penerima         = $request->jumlah_penerima;
         $usulan->keterangan              = $request->keterangan;
         $usulan->save();

         return redirect()->route('sukses', ['message' => 'Data usulan sukses terekam. Terima kasih atas partisipasi Anda!']);
     }

}
