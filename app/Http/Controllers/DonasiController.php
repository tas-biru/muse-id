<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\User;
use App\Role;
use App\Barang;
use App\Makanan;
use Illuminate\Http\Request;

/**
 *
 */
 class DonasiController extends Controller
 {
     public function formulirBarang() {
         if (auth()->user() == null) {return redirect()->route('login');}
         $role = auth()->user()->roles()->first()->name;
         if ($role == 'administrator' || $role == 'donatur') {
             return view('pages.donasibarang', ['user' => auth()->user()]);
         } else {
             return redirect()->route('sukses', ['message' => 'Harus login sebagai donatur!']);
         }
     }

     public function formulirMakanan() {
         if (auth()->user() == null) {return redirect()->route('login');}
         $role = auth()->user()->roles()->first()->name;
         if ($role == 'administrator' || $role == 'donatur') {
             return view('pages.donasipangan', ['user' => auth()->user()]);
         } else {
             return redirect()->route('sukses', ['message' => 'Harus login sebagai donatur!']);
         }
     }

     public function submitBarang(Request $request)
     {
         if (auth()->user() == null) {return redirect()->route('login');}
         $role = auth()->user()->roles()->first()->name;
         if ($role == 'administrator' || $role == 'donatur') {
             $user = auth()->user();
             $barang = new Barang;
             $barang->jenis     = $request->jenis;
             $barang->jumlah    = $request->jumlah;
             $barang->keterangan = $request->keterangan;
             $barang->nama_donatur  = $request->nama_donatur;
             $barang->telepon  = $request->telepon;
             $barang->alamat  = $request->alamat;
             $barang->email  = $request->email;
             $barang->save();

             return redirect()->route('sukses', ['message' => 'Selamat! Anda telah membantu orang lain.']);
         } else {
             return redirect()->route('sukses', ['message' => 'Harus login sebagai donatur!']);
         }
     }

     public function submitMakanan(Request $request)
     {
         if (auth()->user() == null) {return redirect()->route('login');}
         $role = auth()->user()->roles()->first()->name;
         if ($role == 'administrator' || $role == 'donatur') {
             $user = auth()->user();
             $makanan = new Makanan;
             $makanan->jenis     = $request->jenis;
             $makanan->jumlah    = $request->jumlah;
             $makanan->kadaluarsa    = $request->kadaluarsa;
             $makanan->keterangan = $request->keterangan;
             $makanan->nama_donatur  = $request->nama_donatur;
             $makanan->telepon  = $request->telepon;
             $makanan->alamat  = $request->alamat;
             $makanan->email  = $request->email;
             $success = $makanan->save();

             return redirect()->route('sukses', ['message' => 'Selamat! Anda telah membantu orang lain.']);
         } else {
             return redirect()->route('sukses', ['message' => 'Harus login sebagai donatur!']);
         }
     }

}
