<?php

namespace App\Http\Controllers\Auth;

use GuzzleHttp\Client;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $role = Role::where('name', $data['role'])->first();

        $user = new User;
        $user->name     = $data['name'];
        $user->email    = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->alamat  = $data['alamat'];
        $user->telepon  = $data['telepon'];
        if ($role == 'penerima') {
            $user->no_kartu_sejahtera = $data['no_kartu_sejahtera'];
            $user->poin = 100;
        }
        $user->save();
        $user->roles()->save($role);

        $client = new Client([
            'base_uri' => 'https://api.thebigbox.id'
        ]);

        $header = array(
            'x-api-key' => 'fyTjkRaTdDLVX1YG9bJX67IugBKzldOx',
            'accept' => 'application/x-www-form-urlencoded',
            'Content-Type' => 'application/x-www-form-urlencoded'
        );

        $url = '/sms-notification/1.0.0/messages';
        if ($role == 'penerima') {
            $response = $client->request("POST", $url, [
                'headers' => [
                    'x-api-key' => 'DyzKvq3uD8j55nev5pyrmHoNi6pdA1Su',
                    'accept' => 'application/x-www-form-urlencoded',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'msisdn' =>  $data['telepon'],
                    'content' => 'Selamat, ' . $data['name'] . '! Kartu sejahtera Anda telah tervalidasi oleh sistem kami dan Anda telah terdaftar di http://muse-id.herokuapp.com/ ! Yuk gunakan poinmu sekarang!'
                ]
            ]);
        } else {
            $response = $client->request("POST", $url, [
                'headers' => [
                    'x-api-key' => 'DyzKvq3uD8j55nev5pyrmHoNi6pdA1Su',
                    'accept' => 'application/x-www-form-urlencoded',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'msisdn' =>  $data['telepon'],
                    'content' => 'Selamat, ' . $data['name'] . '! Anda sudah terdaftar di http://muse-id.herokuapp.com/ . Yuk, salurkan barang-barang tak terpakaimu agar berguna untuk orang lain!'
                ]
            ]);
        }

        return $user;
    }

    public function pilihRole()
    {
        return view('auth.registerRole');
    }

    public function formulir($role)
    {
        if ($role == "donatur") {
            return view('auth.registerDonatur');
        } elseif ($role == "penerima") {
            return view('auth.registerPenerima');
        } else {
            return redirect()->route('sukses', ['message' => 'Masukkan input yang benar!']);
        }
    }
}
