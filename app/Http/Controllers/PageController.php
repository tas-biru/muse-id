<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\User;
use App\Role;
use Illuminate\Http\Request;

/**
 *
 */
 class PageController extends Controller
 {

     public function halamanDepan() {
         return view('pages.home');
     }

     public function sukses($message) {
         return view('pages.success', ['message' => $message]);
     }

}
