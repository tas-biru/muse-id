<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usulan extends Model
{
    protected $table = 'usulan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_lokasi', 'alamat', 'nama_narahubung',
        'telepon', 'jumlah_penerima', 'keterangan'
    ];

}
