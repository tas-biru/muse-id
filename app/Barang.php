<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{

    protected $table = 'barang';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jenis', 'jumlah', 'keterangan',
        'nama_donatur', 'telepon', 'alamat',
        'email'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
