<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Makanan extends Model
{

    protected $table = 'makanan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jenis', 'jumlah', 'kadaluarsa', 'keterangan',
        'nama_donatur', 'telepon', 'alamat',
        'email'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
