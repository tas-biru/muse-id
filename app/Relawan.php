<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relawan extends Model
{

    protected $table = 'relawan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
        'alamat', 'telepon',
    ];

}
