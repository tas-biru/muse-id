<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis');
            $table->integer('jumlah');
            $table->string('keterangan');
            $table->string('nama_donatur');
            $table->string('telepon');
            $table->string('alamat');
            $table->string('email');
            $table->string('url_foto')->nullable();
            $table->string('poin')->nullable();
            $table->boolean('terkonfirmasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
