<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_administrator = new Role();
        $role_administrator->name = 'administrator';
        $role_administrator->description = 'Administrator';
        $role_administrator->save();

        $role_donatur = new Role();
        $role_donatur->name = 'donatur';
        $role_donatur->description = 'Donatur';
        $role_donatur->save();

        $role_penerima = new Role();
        $role_penerima->name = 'penerima';
        $role_penerima->description = 'Penerima';
        $role_penerima->save();

        // $role_relawan = new Role();
        // $role_relawan->name = 'relawan';
        // $role_relawan->description = 'Relawan';
        // $role_relawan->save();
    }
}
