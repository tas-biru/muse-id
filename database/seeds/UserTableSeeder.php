<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Student;
use App\Parents;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_administrator = Role::where('name', 'administrator')->first();
        $role_donatur = Role::where('name', 'donatur')->first();
        $role_penerima = Role::where('name', 'penerima')->first();
        $role_relawan = Role::where('name', 'relawan')->first();

        $administrator = new User();
        $administrator->name = 'Muse.id Administrator';
        $administrator->email = 'administrator@muse.id';
        $administrator->password = bcrypt('secret');
        $administrator->alamat = 'jl merdeka utara';
        $administrator->telepon = '08123456789';
        $administrator->save();
        $administrator->roles()->attach($role_administrator);

        $donatur = new User();
        $donatur->name = 'Donatur Test';
        $donatur->email = 'donatur@muse.id';
        $donatur->password = bcrypt('secret');
        $donatur->alamat = 'jl merdeka selatan';
        $donatur->telepon = '08123456789';
        $donatur->save();
        $donatur->roles()->attach($role_donatur);

        $penerima = new User();
        $penerima->name = 'Penerima Test';
        $penerima->email = 'penerima@muse.id';
        $penerima->password = bcrypt('secret');
        $penerima->alamat = 'jl merdeka barat';
        $penerima->telepon = '08123456789';
        $penerima->no_kartu_sejahtera = '1234567890';
        $penerima->poin = 100;
        $penerima->approved = True;
        $penerima->save();
        $penerima->roles()->attach($role_penerima);

        // $relawan = new User();
        // $relawan->name = 'Relawan Test';
        // $relawan->email = 'relawan@muse.id';
        // $relawan->password = bcrypt('secret');
        // $relawan->alamat = 'jl merdeka timur';
        // $relawan->telepon = '08123456789';
        // $relawan->save();
        // $relawan->roles()->attach($role_relawan);
    }
}
