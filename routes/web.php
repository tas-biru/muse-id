<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.home');
// });

Route::get('/', 'PageController@halamanDepan')->name('halamanDepan');
Route::get('/sukses/{message}', 'PageController@sukses')->name('sukses');

Auth::routes();

Route::group(['prefix' => 'register', 'as' => 'register.'], function () {
    Route::get('/daftar', 'Auth\RegisterController@pilihRole')->name('pilihRole');
    Route::get('/daftar/{role}', 'Auth\RegisterController@formulir')->name('formulir');
});

Route::group(['prefix' => 'toko', 'as' => 'toko.'], function () {
    Route::get('/', 'TokoController@cari')->name('cari');
    Route::get('/kategori/{kategori}', 'TokoController@cariKategori')->name('cari.kategori');
    Route::get('/pencarian/{keyword}', 'TokoController@cariKeyword')->name('cari.keyword');
    Route::get('/urutkan/{kategori}/{urutan}', 'TokoController@cariUrutkan')->name('cari.urutkan');
    Route::post('/tukar/{barang}', 'TokoController@tukar')->name('tukar');
});

Route::group(['prefix' => 'relawan', 'as' => 'relawan.'], function () {
    Route::get('/', 'RelawanController@tentang')->name('tentang');
    Route::get('/formulir', 'RelawanController@formulir')->name('formulir');
    Route::post('/daftar', 'RelawanController@daftar')->name('daftar');
});

Route::group(['prefix' => 'donasi', 'as' => 'donasi.'], function () {
    Route::get('/formulir/barang', 'DonasiController@formulirBarang')->name('formulir.barang');
    Route::get('/formulir/makanan', 'DonasiController@formulirMakanan')->name('formulir.makanan');
    Route::post('/submit/barang', 'DonasiController@submitBarang')->name('submit.barang');
    Route::post('/submit/makanan', 'DonasiController@submitMakanan')->name('submit.makanan');
});

Route::group(['prefix' => 'usul', 'as' => 'usul.'], function () {
    Route::get('/formulir', 'UsulController@formulir')->name('formulir');
    Route::post('/submit', 'UsulController@submit')->name('submit');
});

Route::get('/sukses-belanja', function () {
     return view('pages.suksesbelanja');
 });

// Route::get('/donasi', 'PageController@donasi')->name('donasi');
// Route::get('/relawan', 'PageController@relawan')->name('relawan');

// Route::get('/donasi-pangan', function () {
//     return view('pages.donasipangan');
// });
//
// Route::get('/donasi-barang', function () {
//     return view('pages.donasibarang');
// });
//
// Route::get('/toko', function () {
//     return view('pages.toko');
// });
//
// Route::get('/relawan', function () {
//     return view('pages.relawan');
// });
//
// Route::get('/usulan-penerima', function () {
//     return view('pages.usulanpenerima');
// });

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/usul-penerima', function () {
    return view('pages.usulpenerima');
});

Route::get('/registrasi-penerima', function () {
    return view('pages.registrasipenerima');
});

Route::get('/registrasi-donatur', function () {
    return view('pages.registrasidonatur');
});

Route::get('/loginn', function () {
    return view('pages.login');
});

//
//
// Auth::routes();
//
// Route::get('/home', 'HomeController@index')->name('home');
